package com.mismirnov.studentsservice.dao;


import com.mismirnov.studentsservice.ConnectionProvider;
import com.mismirnov.studentsservice.domain.Course;

import java.sql.*;


public class CourseDao implements BaseDao<Course> {

    private static final String INSERT = "INSERT INTO courses (course_name, course_description) VALUES (?, ?)";

    private ConnectionProvider connectionProvider;

    public CourseDao(ConnectionProvider connectionProvider) {
        this.connectionProvider = connectionProvider;
    }

    public void create(Course course) {
        try (Connection connection = connectionProvider.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, course.getName());
            preparedStatement.setString(2, course.getDescription());
            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                course.setId(resultSet.getLong(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}