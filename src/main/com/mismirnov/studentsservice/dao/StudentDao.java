
package com.mismirnov.studentsservice.dao;

import com.mismirnov.studentsservice.ConnectionProvider;
import com.mismirnov.studentsservice.domain.Student;

import java.sql.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class StudentDao implements BaseDao<Student> {

    private static final String INSERT = "INSERT INTO students (first_name, last_name) VALUES (?, ?)";
    private static final String DELETE = "DELETE FROM students WHERE student_id = ?";
    private static final String GET_STUDENTS_BY_COURSE_NAME =
            "SELECT students.student_id, students.group_id, students.first_name, students.last_name " +
                    "FROM students_courses " +
                    "INNER  JOIN students " +
                    "ON students_courses.student_id = students.student_id " +
                    "INNER  JOIN courses " +
                    "ON students_courses.course_id = courses.course_id " +
                    "WHERE courses.course_name = ?";
    private static final String ASSIGN_STUDENT_TO_COURSE = "INSERT INTO students_courses (student_id, course_id) VALUES (?, ?)";
    private static final String DELETE_STUDENT_FROM_COURSE = "DELETE FROM students_courses WHERE student_id = ? AND course_id = ?";

    private ConnectionProvider connectionProvider;

    public StudentDao(ConnectionProvider connectionProvider) {
        this.connectionProvider = connectionProvider;
    }

    public void create(Student student) {
        try (Connection connection = connectionProvider.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, student.getFirstName());
            preparedStatement.setString(2, student.getLastName());
            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                student.setId(resultSet.getLong(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteStudentById(int studentId) {
        try (Connection connection = connectionProvider.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(DELETE)) {
            preparedStatement.setInt(1, studentId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public List<Student> getStudentsByCourseName(String courseName) {
        List<Student> result = new ArrayList<>();
        try (Connection connection = connectionProvider.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(GET_STUDENTS_BY_COURSE_NAME)) {
            preparedStatement.setString(1, courseName);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    result.add(new Student(resultSet.getInt(1), resultSet.getString(3), resultSet.getString(4), resultSet.getInt(2)));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public void assignToCourse(long studentId, long courseId) {
        try (Connection connection = connectionProvider.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(ASSIGN_STUDENT_TO_COURSE)) {
            preparedStatement.setLong(1, studentId);
            preparedStatement.setLong(2, courseId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteFromCourse(int studentId, int courseId) {
        try (Connection connection = connectionProvider.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(DELETE_STUDENT_FROM_COURSE)) {
            preparedStatement.setLong(1, studentId);
            preparedStatement.setLong(2, courseId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
