package com.mismirnov.studentsservice.dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.mismirnov.studentsservice.ConnectionProvider;
import com.mismirnov.studentsservice.domain.Group;


public class GroupDao implements BaseDao<Group> {

    private static final String INSERT_GROUPS = "INSERT INTO groups (group_name) VALUES (?)";
    private static final String GET_GROUPS_BY_COUNT =
            "SELECT groups.group_name, COUNT(students.student_id) " +
                    "FROM groups " +
                    "LEFT JOIN students " +
                    "ON students.group_id = groups.group_id " +
                    "GROUP BY groups.group_id " +
                    "HAVING COUNT(*) <= ?";

    private ConnectionProvider connectionProvider;

    public GroupDao(ConnectionProvider connectionProvider) {
        this.connectionProvider = connectionProvider;
    }

    public void create(Group group) {
        try (Connection connection = connectionProvider.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(INSERT_GROUPS, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, group.getName());
            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                group.setId(resultSet.getLong(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Group> getGroupsByStudentsCount(int count) {
        List<Group> result = new ArrayList<>();
        try (Connection connection = connectionProvider.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(GET_GROUPS_BY_COUNT)) {
            preparedStatement.setInt(1, count);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    result.add(new Group(resultSet.getString(1)));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
}