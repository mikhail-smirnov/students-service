package com.mismirnov.studentsservice.dao;

public interface BaseDao<T> {

    public void create(T domain);
}
