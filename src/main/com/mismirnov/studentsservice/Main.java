package com.mismirnov.studentsservice;


import com.mismirnov.studentsservice.dao.BaseDao;
import com.mismirnov.studentsservice.domain.Course;
import com.mismirnov.studentsservice.domain.Group;
import com.mismirnov.studentsservice.domain.Student;
import com.mismirnov.studentsservice.userinterface.UserInterface;
import com.mismirnov.studentsservice.dao.CourseDao;
import com.mismirnov.studentsservice.dao.GroupDao;
import com.mismirnov.studentsservice.dao.StudentDao;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws SQLException, IOException {
        ConnectionProvider connectionProvider = new ConnectionProvider("jdbc:postgresql://localhost:5432/postgres", "postgres", "admin");
        executeScript(connectionProvider, "createUser.sql");
        ConnectionProvider universityConnectionProvider = new ConnectionProvider("jdbc:postgresql://localhost:5432/university", "admin", "admin");
        executeScript(universityConnectionProvider, "createTables.sql");
        TestDataGenerator testDataGenerator = new TestDataGenerator();
        GroupDao groupdao = new GroupDao(universityConnectionProvider);
        List<Group> groups = testDataGenerator.generateGroups();
        create(groupdao, groups);
        CourseDao coursedao = new CourseDao(universityConnectionProvider);
        List<Course> courses = testDataGenerator.generateCourses();
        create(coursedao, courses);
        StudentDao studentDao = new StudentDao(universityConnectionProvider);
        List<Student> students = testDataGenerator.generateStudents(groups);
        create(studentDao, students);
        Map<Student, List<Course>> studentsCourses = testDataGenerator.generateStudentsCourses(students, courses);
        create(studentsCourses, studentDao);
        UserInterface userInterface = new UserInterface(new Scanner(System.in), groupdao, studentDao);
        userInterface.runInterface();
        executeScript(connectionProvider, "dropDatabase.sql");
    }

    private static void executeScript(ConnectionProvider connectionProvider, String scriptPath) throws SQLException, UnsupportedEncodingException, FileNotFoundException {
        ClassLoader classloader = Main.class.getClassLoader();
        try (Connection connection = connectionProvider.getConnection()) {
            ScriptRunner scriptRunner = new ScriptRunner();
            scriptRunner.runScript(connection, URLDecoder.decode(classloader.getResource(scriptPath).getFile(), "UTF-8"));
        }
    }

    private static <T> void  create(BaseDao baseDao, List<T> objects) {
        for (T object : objects) {
            baseDao.create(object);
        }
    }

        private static void create(Map <Student, List <Course>> studentsCourses, StudentDao studentDao){
        for (Map.Entry<Student, List<Course>> entry : studentsCourses.entrySet()) {
            Student student = entry.getKey();
            for (Course course : entry.getValue()) {
                studentDao.assignToCourse(student.getId(), course.getId());
            }
        }
    }
}