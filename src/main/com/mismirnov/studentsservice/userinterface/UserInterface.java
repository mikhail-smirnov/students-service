package com.mismirnov.studentsservice.userinterface;

import com.mismirnov.studentsservice.dao.GroupDao;
import com.mismirnov.studentsservice.dao.StudentDao;
import com.mismirnov.studentsservice.domain.Group;
import com.mismirnov.studentsservice.domain.Student;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class UserInterface {

    private Scanner scanner;
    private GroupDao groupDao;
    private StudentDao studentDao;

    public UserInterface(Scanner scanner, GroupDao groupDao, StudentDao studentDao) {
        this.scanner = scanner;
        this.groupDao = groupDao;
        this.studentDao = studentDao;
    }

    public void runInterface() {
        final AtomicBoolean exit = new AtomicBoolean(false);
        Map<String, Runnable> commands = new HashMap<>();
        commands.put("a", () -> findGroups());
        commands.put("b", () -> findStudentsByCourseName());
        commands.put("c", () -> addNewStudent());
        commands.put("d", () -> deleteStudentById());
        commands.put("e", () -> addStudentToCourse());
        commands.put("f", () -> removeStudentCourse());
        commands.put("g", () -> exit.set(true));
        while (!exit.get()) {
            printMenu();
            commands.get(scanner.next()).run();
        }
        scanner.close();
    }

    private void printMenu() {
        System.out.println();
        System.out.println("a. Find all groups with less or equals student count");
        System.out.println("b. Find all students related to course with given name");
        System.out.println("c. Add new student");
        System.out.println("d. Delete student by STUDENT_ID");
        System.out.println("e. Add a student to the course (from a list)");
        System.out.println("f. Remove the student from one of his or her courses");
        System.out.println("g. Exit program");
        System.out.print("Enter menu-letter >>> ");
    }

    private void findGroups() {
        System.out.println("Find all groups by students count: ");
        System.out.println("Enter students count >>> ");
        int studentsCount = enterNumber();
        List<Group> groups = groupDao.getGroupsByStudentsCount(studentsCount);
        printGroups(groups);
    }

    private void findStudentsByCourseName() {
        System.out.println("Find students by course name: ");
        System.out.print("Enter course name >>> ");
        String courseName = scanner.next();

        List<Student> students = studentDao.getStudentsByCourseName(courseName);

        System.out.println("Student from course \"" + courseName + "\":");
        System.out.println();
        printStudents(students);
    }

    private void addNewStudent() {
        System.out.println("Add new Student: ");
        System.out.println("Enter firstname >>> ");
        String firstName = scanner.next();
        System.out.println("Enter lastname >>> ");
        String lastName = scanner.next();
        studentDao.create(new Student(firstName, lastName));
    }

    private void deleteStudentById() {
        System.out.println("Delete student by id: ");
        int studentId = enterNumber();
        studentDao.deleteStudentById(studentId);
    }

    private void addStudentToCourse() {
        System.out.println("Add student to course: ");
        System.out.println("List of students: ");
        System.out.println("Enter id >>> ");
        int studentId = enterNumber();
        System.out.println("Enter course id >>> ");
        int courseId = enterNumber();
        if (studentId > 0 && courseId > 0) {
            studentDao.assignToCourse(studentId, courseId);
        } else {
            System.out.println("Wrong ID");
        }
    }

    private void removeStudentCourse() {
        System.out.println("Remove student course: ");
        System.out.println("Enter student id >>> ");
        int studentId = enterNumber();
        System.out.println("Enter course id >>> ");
        int courseId = enterNumber();
        if (studentId > 0 && courseId > 0) {
            studentDao.deleteFromCourse(studentId, courseId);
        } else {
            System.out.println("Wrong ID");
        }

    }

    private int enterNumber() {
        int number = 0;
        while (number == 0) {
            number = scanner.nextInt();
            System.out.println("Number entered: " + number);
        }
        return number;
    }

    private void printGroups(List<Group> groups) {
        groups.forEach(group -> System.out.println("Group: " + group.getName()));
    }

    private void printStudents(List<Student> students) {
        students.forEach(student -> System.out.println("ID: " + student.getId() + " Student name: " + student.getFirstName() + " " + student.getLastName()));
    }
}