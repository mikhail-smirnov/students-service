package com.mismirnov.studentsservice;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.IntStream;

import com.github.javafaker.Faker;
import com.mismirnov.studentsservice.domain.Course;
import com.mismirnov.studentsservice.domain.Group;
import com.mismirnov.studentsservice.domain.Student;


public class TestDataGenerator {

    public List<Group> generateGroups() {
        List<Group> groups = new ArrayList<>();
        for (int i = 0; i < 10; ++i) {
            StringBuilder name = new StringBuilder();
            groups.add(new Group(name.append(getRandomCharacter()).append(getRandomCharacter()).append("-").append(getRandomNumber(50)).append(getRandomNumber(50)).toString()));
        }
        return groups;
    }

    public List<Student> generateStudents(List<Group> groups) {
        List<Student> students = generateStudentsWithNames(200);
        List<Student> result = assignStudentsToGroups(students, groups);
        return result;
    }

    private List<Student> generateStudentsWithNames(int studentsCount) {
        List<Student> list = new ArrayList<>();
        List<String> firstNames = generateRandomFirstName();
        List<String> lastNames = generateRandomLastName();
        IntStream.range(1, studentsCount + 1).
                forEach(student -> list.add(new Student(getRandomName(firstNames), getRandomName(lastNames))));
        return list;
    }

    public Map<Student, List<Course>> generateStudentsCourses(List<Student> students, List<Course> courses) {
        Map<Student, List<Course>> result = new HashMap<>();
        for (Student student : students) {
            int coursesCount = getRandomCoursesCount();
            List<Course> courseTmp = new ArrayList<>(courses);
            List<Course> studentCourse = new ArrayList<>();
            for (int i = 0; i < coursesCount; i++) {
                int randomCourseIndex = getRandomCourseIndex(courseTmp);
                Course course = courseTmp.get(randomCourseIndex);
                studentCourse.add(course);
                courseTmp.remove(randomCourseIndex);
            }
            result.put(student, studentCourse);
        }
        return result;
    }

    public List<Course> generateCourses() {
        List<Course> list = new ArrayList<>();
        list.add(new Course("Financial Engineering and Risk Management", "Financial Engineering is a multidisciplinary field drawing from finance and economics, mathematics, statistics, engineering and computational methods. The emphasis of FE & RM Part I will be on the use of " +
                "simple stochastic models to price derivative securities in various asset classes including equities, fixed income, credit and mortgage-backed securities. We will also consider the role that some of these asset classes played during the financial crisis. "));
        list.add(new Course("Financial Markets", "An overview of the ideas, methods, and institutions that permit human society to manage risks and foster enterprise.  Emphasis on financially-savvy leadership skills. Description of practices today and analysis of prospects for the future. " +
                "Introduction to risk management and behavioral finance principles to understand the real-world functioning of securities, insurance, and banking industries.  The ultimate goal of this course is using such industries effectively and towards a better society."));
        list.add(new Course("Academic English", ""));
        list.add(new Course("Robotics", ""));
        list.add(new Course("Inferential Statistics", ""));
        list.add(new Course("Dino 101: Dinosaur Paleobiology", "Dino 101: Dinosaur Paleobiology is a 12-lesson course teaching a comprehensive overview of non-avian dinosaurs. Topics covered: anatomy, eating, locomotion, growth, environmental and behavioral adaptations, origins and extinction. " +
                "Lessons are delivered from museums, fossil-preparation labs and dig sites. Estimated workload: 3-5 hrs/week."));
        list.add(new Course("Strategic Leadership and Management", "Enhance leadership and business skills for immediate impact. Practice everyday leadership, manage people, learn and apply concepts and techniques to effectively manage organizations through organizational design, and formulate and " +
                "implement strategy."));
        list.add(new Course( "Linear algebra", ""));
        list.add(new Course( "Deep Learning", ""));
        list.add(new Course( "Linear Regression for Business Statistics", ""));
        return list;
    }

    private List<Student> assignStudentsToGroups(List<Student> students, List<Group> groups) {
        List<Student> randomStudents = new ArrayList<>(students);
        for (Group group : groups) {
            int studentsCount = getRandomStudentsCount();
            for (int i = 0; i < studentsCount; i++) {
                if (studentsCount > students.size() || !randomStudents.isEmpty()) {
                    int randomStudentIndex = getRandomStudentIndex(randomStudents);
                    long studentId = randomStudents.get(randomStudentIndex).getId();
                    randomStudents.remove(randomStudentIndex);
                    assignGroupIdToStudent(students, studentId, group.getId());
                }
            }
        }
        return students;
    }

    private void assignGroupIdToStudent(List<Student> students, long studentId, long groupId) {
        for (Student student : students) {
            if (student.getId() == studentId) {
                student.setGroupId(groupId);
            }
        }
    }

    private char getRandomCharacter() {
        Random random = new Random();
        char character = (char) (random.nextInt(26) + 'a');
        return character;
    }

    private int getRandomNumber(int upperLimit) {
        return ((int) (Math.random() * upperLimit + 1));
    }


    private List<String> generateRandomFirstName() {
        Faker faker = new Faker();
        List<String> firstNames = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            firstNames.add(faker.name().firstName());
        }
        return firstNames;
    }

    private List<String> generateRandomLastName() {
        Faker faker = new Faker();
        List<String> lastNames = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            lastNames.add(faker.name().lastName().replace("\'", ""));
        }
        return lastNames;
    }

    private String getRandomName(List<String> names) {
        int randIndex = new Random().nextInt(names.size());
        return names.get(randIndex);
    }


    private int getRandomStudentsCount() {
        int[] studentsInGroup = {0, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
                21, 22, 23, 24, 25, 26, 27, 28, 29, 30};
        Random random = new Random();
        return studentsInGroup[random.nextInt(studentsInGroup.length - 1)];
    }

    private int getRandomStudentIndex(List<Student> students) {
        Random random = new Random();
        if (students.size() > 0) {
            return random.nextInt(students.size());
        } else return 0;
    }

    private int getRandomCoursesCount() {
        Random random = new Random();
        return random.nextInt(3) + 1;
    }

    private int getRandomCourseIndex(List<Course> cours) {
        Random random = new Random();
        return random.nextInt(cours.size());
    }
}

