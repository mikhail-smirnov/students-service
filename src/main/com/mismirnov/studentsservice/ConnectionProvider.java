package com.mismirnov.studentsservice;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionProvider {

    final private String user;
    final private String password;
    final private String url;

    public ConnectionProvider(String url, String user, String password) {
        this.user = user;
        this.password = password;
        this.url = url;
    }

    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection(url, user, password);
    }
}

