package com.mismirnov.studentsservice.domain;


public class Student {

    private long id;
    private String firstName;
    private String lastName;
    private long groupId;

    public Student(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Student(long id, String firstName, String lastName, long groupId) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.groupId = groupId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public long getId() {
        return id;
    }

    public void setGroupId(long groupId) {
        this.groupId = groupId;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "UsersDataSet{" +
                "id=" + id +
                ", name='" + firstName + '\'' +
                '}';
    }
}