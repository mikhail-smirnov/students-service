package com.mismirnov.studentsservice;

import java.io.*;
import java.sql.*;
import java.util.Scanner;

public class ScriptRunner {

    public void runScript(Connection connection, String scriptPath) throws SQLException, FileNotFoundException {
        FileReader fileReader = new FileReader(scriptPath);
        Scanner fileScanner = new Scanner(fileReader);
        fileScanner.useDelimiter("(;(\r)?\n)|(--\n)");
        try (Statement statement = connection.createStatement()) {
            while (fileScanner.hasNext()) {
                String line = fileScanner.next();
                if (line.startsWith("/*!") && line.endsWith("*/")) {
                    int i = line.indexOf(' ');
                    line = line.substring(i + 1, line.length() - " */".length());
                }
                if (line.trim().length() > 0) {
                    statement.executeUpdate(line);
                }
            }
        }
    }
}